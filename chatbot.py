import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer

class chatbot:

    def __init__(self, corpus='dialogs.txt', similarity_treshold=0.5):
        self.similarity_treshold = similarity_treshold
        self.df = pd.read_csv(corpus, header=None, delimiter='\t')
        self.vec = TfidfVectorizer(lowercase=True)
        self.X = self.vec.fit_transform(self.df[0])
        self.question = self.X.toarray()

    def get_response(self, text):
        self. exit_list = ['exit', 'see you later', 'bye', 'quit', 'break', 'stop']

        response = None
        text = text.lower()
        if text in self.exit_list:
            response = 'Bot: Chat with you later, stay safe!!'
        else:
            text_vec = self.vec.transform([text])
            text_vec = text_vec.toarray()

            text_vec = text_vec[0].reshape(1, -1) 
            similarity = cosine_similarity(text_vec, self.question)
            
            if (max(similarity[0]) > self.similarity_treshold):
                answer_index = np.argmax(similarity[0])
                answer = self.df[1].iloc[answer_index]
                response = answer
            else:
                response = 'Sorry, i dont understand your words'

        return response


        
        