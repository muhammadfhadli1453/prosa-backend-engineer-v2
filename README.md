# Prosa Backend Engineer V2

This is the 2nd version of Real-time chatbot, developed using Flask for Entrance Task of Prosa's Backend Engineer Candidate

**Accomplished:**
1. Conversational agent web-app
2. Comparison of question is done by using Cosine Similarity from Scikit-learn
3. QA pairs are stored in a text file named _dialogues.txt_
4. Real-time chatbot

**Not yet accomplished:**
1. Time-out for close the conversation: <br> I confuse where to put the time-out function. I have tried to use `time.sleep` and `multiprocessing` and put in the `get_bot_response()` but the code is not working

**To run the code** <br>
If you are using Anaconda, the steps to run this code are:
1. Install requirements on Terminal
`conda create --name <env> --file requirements.txt`
2. run app.py
`python app.py`
3. Open your server using web browser on address:
`127.0.0.1:5000/`

